from django.contrib import admin
from .models import Chamada

class ChamadaAdmin(admin.ModelAdmin):
    list_display = ['destaque', 'titulo', 'atualizacao']
    list_display_links = ['titulo']

admin.site.register(Chamada, ChamadaAdmin)
