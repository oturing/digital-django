
from django.conf.urls import patterns, include, url
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView

from .views import HomePageView
from .models import Chamada

urlpatterns = patterns('',
    url(r'^$', HomePageView.as_view(), name='home'),
    url(r'contribuir/', CreateView.as_view(model=Chamada), name='contribuir'),
    url(r'site/(?P<pk>\d+)/', DetailView.as_view(model=Chamada), name='detalhe-chamada')
)
